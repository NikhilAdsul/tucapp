package com.tuc.loginservice.utils;

import com.tuc.loginservice.beans.LoginBean;
import com.tuc.loginservice.entities.Response;
import com.tuc.loginservice.entities.User;
import org.springframework.http.HttpStatus;

import java.util.regex.Pattern;

public class Common {


    public static boolean emailValidation(LoginBean loginBean) {
        if (loginBean!=null && isValidEmail(loginBean.getEmail ()))
            return true;
        return false;
    }


    public static boolean isValidEmail(String email)
    {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null || email.isEmpty())
            return false;
        return pat.matcher(email).matches();
    }

   public static Response MakeSuccessResponse(String message, String status, HttpStatus statusCode, boolean isSuccess, Object data) {

        Response response = new Response();
        response.setMessage(message);
        response.setStatus(status);
        response.setStatusCode(statusCode);
        response.setIsSuccess(isSuccess);
        response.setData(data);
        return response;
    }
}

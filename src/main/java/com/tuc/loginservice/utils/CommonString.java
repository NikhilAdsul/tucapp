package com.tuc.loginservice.utils;

public class CommonString {

    public static String LOGIN_SUCCESS_MSG="login successfully...";
    public static String LOGIN_FAILURE_MSG="login failed... try again";

    public static String SERVER_DOWN_ERROR="Server is down,try again...";
    public static String REGISTER_SUCCESS_MSG="Register successfully...";
    public static String REGISTER_FAILURE_MSG="Register failed... try again";

    public static String INVALID_EMAILID="Please Enter Valid Email Id";
    public static String INVALID_PASSWORD="Please Enter Valid Password";


    public static String EMAIL_EXITS="Email Id already exits... try another";


    public static String SUCCESS="Success";
    public static String FAILURE="Failure";

}
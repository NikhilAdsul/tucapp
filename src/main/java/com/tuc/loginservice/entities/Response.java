package com.tuc.loginservice.entities;

import org.springframework.http.HttpStatus;

public class Response {


    private String status;

    private String message;

    private boolean isSuccess;

    private Object data;

    private HttpStatus statusCode;


    //Old Code

    private String accessToken;

    private String serverProfilePicUrl;

    private String userEmail;


    private Long userId;

    private String userName;

    public Response() {

    }


    public Response(String status, String message, boolean isSuccess, Object data, HttpStatus statusCode) {
        this.status = status;
        this.message = message;
        this.isSuccess = isSuccess;
        this.data = data;
        this.statusCode = statusCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(HttpStatus statusCode) {
        this.statusCode = statusCode;
    }


}
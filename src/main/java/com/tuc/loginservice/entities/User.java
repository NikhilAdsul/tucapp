package com.tuc.loginservice.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(generator = "g", strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;


    @NonNull
    @NotEmpty
    @Column(name = "name")
    @JsonProperty("name")
    private String name;

    @NonNull
    @NotEmpty
    @Column(name = "email")
    @JsonProperty("email")
    private String email;

    @NonNull
    @NotEmpty
    @Pattern(regexp = "^[0-9]{10}$")
    @Column(name = "phoneNumber")
    @JsonProperty("phonenumber")
    private String phoneNumber;


    @NonNull
    @NotEmpty
    @Column(name = "password")
    @JsonProperty("password")
    private String password;

    @NonNull
    @Column(name = "confirmPssword")
    @JsonProperty("confirmpassword")
    private String confirmPssword;

    @NonNull
    @NotEmpty
    @Column(name = "userRole")
    @JsonProperty("userrole")
    private String userRole;

    @NonNull
    @NotEmpty
    @Column(name = "roleId")
    @JsonProperty("roleid")
    @JsonIgnore
    private String roleId;

    public User() {

    }

    public User(String name, String email, String phoneNumber, String password, String confirmPssword, String userRole, String roleId) {
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.confirmPssword = confirmPssword;
        this.userRole = userRole;
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPssword() {
        return confirmPssword;
    }

    public void setConfirmPssword(String confirmPssword) {
        this.confirmPssword = confirmPssword;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", password='" + password + '\'' +
                ", confirmPssword='" + confirmPssword + '\'' +
                ", userRole='" + userRole + '\'' +
                ", roleId='" + roleId + '\'' +
                '}';
    }
}

package com.tuc.loginservice.service;

import com.tuc.loginservice.beans.LoginBean;
import com.tuc.loginservice.daos.LoginRepository;
import com.tuc.loginservice.entities.Response;
import com.tuc.loginservice.entities.User;
import com.tuc.loginservice.utils.Common;
import com.tuc.loginservice.utils.CommonString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import sun.rmi.runtime.Log;

import java.util.ArrayList;
import java.util.List;

@Service
public class LoginServiceImpl implements LoginService {


    @Autowired
    LoginRepository loginRepository;


    @Override
    public Response loginAuthService(LoginBean loginBean) {

        List<User> userList = new ArrayList<User>();
        User userResponse;

        try {
            if (Common.emailValidation(loginBean)) {
                userResponse = loginRepository.loginAuthRepository(loginBean);
                if (loginBean.getPassword().equals(userResponse.getPassword())) {
                    userList.add(userResponse);
                    return Common.MakeSuccessResponse(CommonString.LOGIN_SUCCESS_MSG,
                            CommonString.SUCCESS,
                            HttpStatus.OK,
                            true, userList);
                } else {
                    return Common.MakeSuccessResponse(CommonString.INVALID_PASSWORD,
                            CommonString.FAILURE,
                            HttpStatus.OK,
                            false, userList);
                }


            } else {
                return Common.MakeSuccessResponse(CommonString.INVALID_EMAILID,
                        CommonString.FAILURE,
                        HttpStatus.OK,
                        false,
                        userList);
            }
        } catch (Exception e) {
            return Common.MakeSuccessResponse(e.getMessage(),
                    CommonString.FAILURE,
                    HttpStatus.OK,
                    false,
                    userList);
        }
    }

    @Override
    public Response userRegistration(User user) {
        List<User> userList = new ArrayList<User>();
        try {

            User userForEmailExits = loginRepository.checkEmailExits(user);
            if (userForEmailExits == null) {
                loginRepository.userRegistration(user);

                userList.add(user);

                return Common.MakeSuccessResponse(CommonString.REGISTER_SUCCESS_MSG,
                        CommonString.SUCCESS,
                        HttpStatus.OK,
                        true,
                        userList);


            } else if (!user.getEmail().equals(userForEmailExits.getEmail())) {

                return Common.MakeSuccessResponse(CommonString.EMAIL_EXITS,
                        CommonString.FAILURE,
                        HttpStatus.OK,
                        false,
                        userList);

            } else {
                return Common.MakeSuccessResponse(CommonString.REGISTER_FAILURE_MSG,
                        CommonString.FAILURE,
                        HttpStatus.OK,
                        false,
                        userList);
            }


        } catch (Exception e) {
            return Common.MakeSuccessResponse(e.getMessage(),
                    CommonString.FAILURE,
                    HttpStatus.OK,
                    false,
                    userList);
        }
    }
}

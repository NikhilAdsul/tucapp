package com.tuc.loginservice.service;

import com.tuc.loginservice.beans.LoginBean;
import com.tuc.loginservice.entities.Response;
import com.tuc.loginservice.entities.User;

public interface LoginService {

    public Response loginAuthService(LoginBean loginBean);
    public Response userRegistration(User user);
  //  public String CheckEmailExits();
}

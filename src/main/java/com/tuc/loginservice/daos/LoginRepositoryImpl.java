package com.tuc.loginservice.daos;

import com.tuc.loginservice.beans.LoginBean;
import com.tuc.loginservice.entities.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation. Autowired;
import org.springframework.stereotype.Repository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Repository
public class LoginRepositoryImpl implements LoginRepository {

    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf) {
        this.sessionFactory = sf;
    }


    @Override
    public User loginAuthRepository(LoginBean loginBean) {
        Session session = this.sessionFactory.getCurrentSession();
        Query<User> q = session.createQuery("from " + User.class.getName() + " user where user.email=:p_email");
        q.setParameter("p_email", loginBean.getEmail());
        return q.getSingleResult();
    }

    @Override
    public void userRegistration(User user) {

        //this.sessionFactory.getCurrentSession().persist(user);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateobj = new Date();
        //System.out.println(df.format(dateobj));

        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery("INSERT INTO user (name, email,phoneNumber,password,confirmPssword,userRole,roleId,last_login_date,reg_date) " +
                "VALUES (?,?,?,?,?,?,?,?,?)");
        /*query.setParameter("name", user.getName());
        query.setParameter("email", user.getEmail());
        query.setParameter("phoneNumber", user.getPhoneNumber());
        query.setParameter("password", user.getPassword());
        query.setParameter("confirmPssword", user.getConfirmPssword());
        query.setParameter("userRole", user.getUserRole());
        query.setParameter("roleId", user.getRoleId());
        query.setParameter("last_login_date", ""*//*df.format(dateobj)*//*);
        query.setParameter("reg_date", df.format(dateobj));*/
        query.setParameter(1, user.getName());
        query.setParameter(2, user.getEmail());
        query.setParameter(3, user.getPhoneNumber());
        query.setParameter(4, user.getPassword());
        query.setParameter(5, user.getConfirmPssword());
        query.setParameter(6, user.getUserRole());
        query.setParameter(7, user.getRoleId());
        query.setParameter(8, df.format(dateobj));
        query.setParameter(9, df.format(dateobj));
        query.executeUpdate();
        session.getTransaction().commit();

    }

    @Override
    public User checkEmailExits(User user) {
        Query query = sessionFactory.getCurrentSession().createQuery("from " + User.class.getName() + " where email = :p_email");
        query.setParameter("p_email", user.getEmail());
        // User userResponse=(User) query.list().get(0);
        return (User) query.uniqueResult();
    }
}

package com.tuc.loginservice.daos;

import com.tuc.loginservice.beans.LoginBean;
import com.tuc.loginservice.entities.User;

public interface LoginRepository {

    public User loginAuthRepository(LoginBean loginBean);
    public void userRegistration(User user);
    public User checkEmailExits(User user);
}

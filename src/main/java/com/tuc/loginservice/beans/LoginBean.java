package com.tuc.loginservice.beans;


import com.tuc.loginservice.entities.User;

public class LoginBean {

    private String email;
    private String password;
    private User user;

    public LoginBean() {
        email = "";
        password = "";
        user = null;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "LoginBean{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", user=" + user +
                '}';
    }
}

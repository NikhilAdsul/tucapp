package com.tuc.loginservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;


//resolved error :
// Failed to configure a DataSource: 'url' attribute is not specified and no embedded datasource could be configured.
//	If you have database settings to be loaded from a particular profile you may need to activate it (no profiles are currently active).
//Reason: Failed to determine a suitable driver class
//@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
@SpringBootApplication
@ComponentScan(basePackages = "com.tuc.loginservice")
public class LoginServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoginServiceApplication.class, args);
    }

}

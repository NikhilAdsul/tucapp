package com.tuc.loginservice.controller;

import com.tuc.loginservice.beans.LoginBean;
import com.tuc.loginservice.entities.Response;
import com.tuc.loginservice.entities.User;
import com.tuc.loginservice.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/tuc")
public class LoginController {


    @Autowired
    LoginService loginService;

    // emailId=Swap&password=Swap

    @PostMapping(value = "/loginAuth", produces = "application/json", consumes = "application/json")
    public Response loginAuth(@RequestBody LoginBean loginBean/*@RequestParam(value = "emailId") String emailId, @RequestParam(value = "password") String password*/) {
       /* LoginBean loginBean = new LoginBean();
        loginBean.setEmail(emailId);
        loginBean.setPassword(password);*/
        return loginService.loginAuthService(loginBean);
    }

    @PostMapping(value = "/userRegistration", produces = "application/json", consumes = "application/json")
    public Response userRegistration(@RequestBody User user) {
        return loginService.userRegistration(user);
    }

    @GetMapping("/welcome")
    public String welcome() {
        System.out.println("Welcome");
        return "Welcome....";
    }


}
